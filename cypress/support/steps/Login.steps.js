/// <reference types="cypress" />

import LoginObject from "../pageObjetcs/LoginObject";

const login = new LoginObject();

Given("Acess page Avenue Code", () => {
  login.acessPage();
});

When("I am logged in", () => {
  login.clickButtonSignIn();
  login.fill_email();
  login.fill_Password();
  login.btn_login();
});

Then("I should see the message {string}", (message) => {
  login.message_welcome(message);
});

import LoginObject from "../pageObjetcs/LoginObject";
import { TasksObject } from "../pageObjetcs/TasksObjects";

const login = new LoginObject();
const tasks = new TasksObject();

Given("i access the task page and see message {string}", (mensagem) => {
  login.acessPage();
  login.clickButtonSignIn();
  login.fill_email();
  login.fill_Password();
  login.btn_login();

  tasks.bnt_tasks();
  tasks.mensagem_tasks_user(mensagem);
});
When("should i add a new task {string}", (Newtasks) => {
  tasks.fill_tasks(Newtasks);
  tasks.add_click_tasks();
});
Then("i should see the task in the task list", () => {
  tasks.click_remove_task();
});

When("create a new subtasks {string}", (subtasks) => {
  tasks.fill_tasks(subtasks);
  tasks.add_click_tasks();
  tasks.click_add_subtasks();
  tasks.text_subtask(subtasks);
  tasks.close_modal();
});

Then("see quantify subtasks in screen", () => {
  tasks.click_remove_taks_root();
});

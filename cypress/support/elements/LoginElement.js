export default class LoginElement {
  FILL_EMAIL = () => {
    return "#user_email";
  };
  FILL_PASSWORD = () => {
    return "#user_password";
  };

  BTN_SIGN = () => {
    return ".btn-primary";
  };
  BNT_LOGIN = () => {
    return ".btn";
  };

  MSG_WELCOME = () => {
    return ".navbar-right > :nth-child(1) > a";
  };
}

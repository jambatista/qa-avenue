export class TasksElement {
  btn_acess_tasks = () => {
    return "#my_task";
  };
  msg_Tasks_welcome = () => {
    return "h1";
  };

  add_new_tasks = () => {
    return "#new_task";
  };
  click_add_tasks = () => {
    return ".input-group-addon";
  };

  click_remove_task = () => {
    return ":nth-child(5) > .btn";
  };
  click_remove_taks_root = () => {
    return ":nth-child(4) > .btn";
  };

  click_subtasks_bnt = () => {
    return ":nth-child(1) > :nth-child(4) > .btn";
  };
  create_subtasks = () => {
    return "#new_sub_task";
  };
  click_add_subtasks = () => {
    return ":nth-child(4) > .btn";
  };
  text_subtask = () => {
    return "#new_sub_task";
  };

  close_modal = () => {
    return ".modal-footer > .btn";
  };
}

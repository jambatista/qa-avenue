/// <reference types="cypress" />

import { TasksElement } from "../elements/TasksElement";

const tasks = new TasksElement();

export class TasksObject {
  bnt_tasks() {
    cy.get(tasks.btn_acess_tasks()).click();
  }
  mensagem_tasks_user(mensagem) {
    cy.contains(mensagem);
  }

  fill_tasks(Newtasks) {
    cy.get(tasks.add_new_tasks()).type(Newtasks);
  }
  click_remove_task() {
    cy.wait(2000);

    cy.get(tasks.click_remove_task()).click();
  }
  click_remove_taks_root() {
    cy.wait(2000);

    cy.get(tasks.click_remove_taks_root()).click();
  }
  add_click_tasks() {
    cy.wait(2000);
    cy.get(tasks.click_add_tasks()).click();
  }

  click_add_subtasks() {
    cy.wait(2000);

    cy.get(tasks.click_add_subtasks()).click();
  }
  text_subtask(subtasks) {
    cy.get(tasks.text_subtask()).type(subtasks);
  }
  close_modal() {
    cy.get(tasks.close_modal()).click();
  }
}

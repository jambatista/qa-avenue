import LoginElement from "../elements/LoginElement";

import USER from "../../fixtures/user.json";
const baseUrl = Cypress.config("baseUrl");

const login = new LoginElement();
export default class LoginObject {
  acessPage() {
    cy.visit(baseUrl);
  }
  clickButtonSignIn() {
    cy.get(login.BTN_SIGN()).click();
  }

  fill_email() {
    cy.get(login.FILL_EMAIL()).type(USER.email);
  }
  fill_Password() {
    cy.get(login.FILL_PASSWORD()).type(USER.password);
  }
  btn_login() {
    cy.get(login.BNT_LOGIN()).click();
  }
  message_welcome(mensagem) {
    cy.get(login.MSG_WELCOME()).should(($p) => {
      expect($p.eq(0)).to.have.contain(mensagem);
    });
  }
}
